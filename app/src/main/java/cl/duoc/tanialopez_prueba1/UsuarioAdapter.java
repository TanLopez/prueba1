package cl.duoc.tanialopez_prueba1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.ArrayList;

import cl.duoc.tanialopez_prueba1.Ent.Usuario;

public class UsuarioAdapter extends ArrayAdapter<Usuario> {

    private ArrayList<Usuario> dataSource;

    public UsuarioAdapter(Context context, ArrayList<Usuario> dataSource){
        super(context, R.layout.item, dataSource);
        this.dataSource = dataSource;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.item, null);

        TextView tvUsuario = (TextView)item.findViewById(R.id.tvUsuario);
        tvUsuario.setText(dataSource.get(position).getUsuario().toString());


        return(item);
    }
}
