package cl.duoc.tanialopez_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.tanialopez_prueba1.Bd.BaseDeDatos;
import cl.duoc.tanialopez_prueba1.Ent.Usuario;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnCrear, btnVolver;
    private EditText edUsuario, edPass1,edPass2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        btnCrear =(Button) findViewById(R.id.btnCrear);
        btnVolver =(Button) findViewById(R.id.btnVolver);

        edUsuario= (EditText) findViewById(R.id.edUsuario);
        edPass1= (EditText) findViewById(R.id.edPass1);
        edPass2= (EditText) findViewById(R.id.edPass2);

        btnCrear.setOnClickListener(this);
        btnVolver.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.btnCrear)
        {
            String Err="";
            if(edUsuario.getText().length()==0)
            {
                Err+="Ingrese nombre de usuario \n";
            }
            if(edPass1.getText().length()==0  )
            {
                Err+="Ingrese clave \n";
            }
            if( edPass2.getText().length()==0)
            {
                Err+= "Ingrese clave para confirmar \n";
            }
            if(!edPass1.getText().toString().equals(edPass2.getText().toString()))
            {
               Err+="Claves no iguales \n";
            }
            if(Err.length()==0){
                    Usuario u = new Usuario();
                    u.setUsuario(edUsuario.getText().toString());
                    u.setPass(edPass1.getText().toString());

                Toast.makeText(this, "Usuario Agregado " + u.getUsuario() + u.getPass() , Toast.LENGTH_SHORT).show();

                BaseDeDatos.agregarUsuario(u);

            }
            else
                {
                    Toast.makeText(this, Err, Toast.LENGTH_SHORT).show();

                }

        }
        if (v.getId()== R.id.btnVolver)
        {
            onBackPressed();


        }

    }
}
