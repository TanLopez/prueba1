package cl.duoc.tanialopez_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import cl.duoc.tanialopez_prueba1.Bd.BaseDeDatos;
import cl.duoc.tanialopez_prueba1.Ent.Usuario;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnRegistrarse, btnEntrar;
    private EditText edPass, edUs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnRegistrarse=(Button)findViewById(R.id.btnRegistrarse);
        btnRegistrarse.setOnClickListener(this);
        btnEntrar=(Button) findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(this);
         edPass= (EditText) findViewById(R.id.edPass);
        edUs=(EditText) findViewById(R.id.edUs);

    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.btnEntrar)
        {

            ArrayList<Usuario> L = BaseDeDatos.obtieneListadoUsuarios();
            if(L.isEmpty())
            {
                Toast.makeText(this, "vacio ", Toast.LENGTH_SHORT).show();

            }
            else
            { Toast.makeText(this, "no vacio ", Toast.LENGTH_SHORT).show();
                if( validar())
                {

                Intent i = new Intent(LoginActivity.this, ListadoUsuariosActivity.class);
                startActivity(i);
                }
            }



        }
        if (v.getId()== R.id.btnRegistrarse)
        {
            Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
            startActivity(i);

        }
    }

    private boolean validar() {
        boolean c = false;
        ArrayList<Usuario> L = BaseDeDatos.obtieneListadoUsuarios();


        for (Usuario x : L) {
            if (x.getUsuario().equals(edUs.getText().toString()) && x.getPass().equals(edPass.getText().toString())) {
                c = true;
            }

        }
         return c;
    }

}




