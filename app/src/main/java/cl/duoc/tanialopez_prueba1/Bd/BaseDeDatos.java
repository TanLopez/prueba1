package cl.duoc.tanialopez_prueba1.Bd;

import cl.duoc.tanialopez_prueba1.Ent.Usuario;
import java.util.ArrayList;

/**
 * Created by DUOC on 20-04-2017.
 */

public class BaseDeDatos {

    public  static ArrayList<Usuario> values = new ArrayList<>();
    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}
