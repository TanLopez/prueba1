package cl.duoc.tanialopez_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import cl.duoc.tanialopez_prueba1.Bd.BaseDeDatos;

public class ListadoUsuariosActivity extends AppCompatActivity {

    private ListView Lis;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);


       Lis = (ListView)findViewById(R.id.Lis);
        UsuarioAdapter adaptador = new UsuarioAdapter(this, BaseDeDatos.obtieneListadoUsuarios());
        Lis.setAdapter(adaptador);

    }
}
